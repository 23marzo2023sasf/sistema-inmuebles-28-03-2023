DROP TABLE IF EXISTS peticion;
DROP TABLE IF EXISTS inmueble;
DROP TABLE IF EXISTS usuario;
USE db_prueba;
CREATE TABLE usuario( 
    id BIGINT PRIMARY KEY AUTO_INCREMENT, 
    email VARCHAR(100) UNIQUE NOT NULL, 
    password VARCHAR(1) NOT NULL,
    roles JSON NOT NULL, 
    nombres VARCHAR(50) NOT NULL, 
    apellidos VARCHAR(50) NOT NULL, 
    tipo VARCHAR(25) NOT NULL, estado VARCHAR(1) );


CREATE TABLE inmueble(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(100) NOT NULL,
    ubicacion VARCHAR(100) NOT NULL,
    precio INT NOT NULL,
    estado VARCHAR(25) NOT NULL,
    estado_base VARCHAR(1) NOT NULL,
    asesor_id INT FOREIGN KEY NOT NULL,
    cliente_id INT FOREIGN KEY,
    tipo VARCHAR(20) NOT NULL
);

CREATE TABLE peticion(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(100) NOT NULL,
    ubicacion VARCHAR(100) NOT NULL,
    precio INT NOT NULL,
    estado VARCHAR(25) NOT NULL,
    estado_base VARCHAR(1) NOT NULL,
    asesor_id INT FOREIGN KEY NOT NULL,
    cliente_id INT FOREIGN KEY,
    tipo VARCHAR(20) NOT NULL
);
    