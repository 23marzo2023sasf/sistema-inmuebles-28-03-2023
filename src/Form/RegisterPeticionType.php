<?php

namespace App\Form;

use App\Entity\Peticion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterPeticionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('descripcion',TextType::class)
            ->add('dudas',TextType::class)
            //->add('estado')
            //->add('estado_base')
            //->add('asesor_id')
            //->add('cliente_id')
            //->add('inmueble_id')
            ->add('tipo',ChoiceType:: class, array(
                'choices' => array(
                    'Venta' => 'Venta',
                    'Alquiler' => 'Alquiler')
                    
                
                ))
                ->add('save', SubmitType :: class, ['label' => 'Guardar']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Peticion::class,
        ]);
    }
}
