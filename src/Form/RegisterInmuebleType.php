<?php

namespace App\Form;

use App\Entity\Inmueble;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterInmuebleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        if($options['accion']=='admin')
        {
            $builder
            ->add('descripcion',TextType::class)
            ->add('ubicacion',TextType::class)
            ->add('precio',NumberType::class)
            ->add('estado',ChoiceType:: class, array(
                'choices' => array(
                    'Adquirido' => 'Adquirido')
                
                ))
           // ->add('estado_base')
           ->add('asesor_id',ChoiceType:: class, array(
            'choices' => array(
                'Juan Daniel' => 2,
                'Oscar Alberto' => 3,
                'Rafael Steven' => 4)
            
            ))
            //->add('cliente_id')
            
            ->add('tipo',ChoiceType:: class, array(
                'choices' => array(
                    'Edificio' => 'Edificio',
                    'Casa' => 'Casa',
                    'Departamento' => 'Departamento')
                
                ))
                ->add('save', SubmitType :: class, ['label' => 'Guardar']);
    }
    if($options['accion']=='asesor')
        {
            $builder
            ->add('descripcion',TextType::class,['disabled'=>true])
            ->add('ubicacion',TextType::class,['disabled'=>true])
            ->add('precio',NumberType::class,['disabled'=>true])
            ->add('estado',ChoiceType:: class, array(
                'choices' => array(
                    'Adquirido' => 'Adquirido',
                    'En venta' => 'En venta',
                    'En alquiler' => 'En alquiler',
                    'Alquilado'=>'Alquilado',
                    'Vendido' => 'Vendido')
                
                ))
           // ->add('estado_base')
           ->add('asesor_id',ChoiceType:: class, array(
            'choices' => array(
                'Juan Daniel' => 2,
                'Oscar Alberto' => 3,
                'Rafael Steven' => 4),
                'disabled'=>true
            
            ))
            //->add('cliente_id')
            
            ->add('tipo',ChoiceType:: class, array(
                'choices' => array(
                    'Edificio' => 'Edificio',
                    'Casa' => 'Casa',
                    'Departamento' => 'Departamento'),
                    'disabled'=>true
                
                ))
                ->add('save', SubmitType :: class, ['label' => 'Guardar']);
    }
        }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Inmueble::class,
            'accion'=>'admin',
        ]);
    }
}
