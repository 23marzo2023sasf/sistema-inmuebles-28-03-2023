<?php

namespace App\Entity;

use App\Repository\PeticionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PeticionRepository::class)]
class Peticion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $descripcion = null;

    #[ORM\Column(length: 100)]
    private ?string $dudas = null;

    #[ORM\Column(length: 25)]
    private ?string $estado = null;

    #[ORM\Column(length: 1)]
    private ?string $estado_base = null;

    #[ORM\Column(nullable: true)]
    private ?int $asesor_id = null;

    #[ORM\Column]
    private ?int $cliente_id = null;

    #[ORM\Column]
    private ?int $inmueble_id = null;

    #[ORM\Column(length: 20)]
    private ?string $tipo = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDudas(): ?string
    {
        return $this->dudas;
    }

    public function setDudas(string $dudas): self
    {
        $this->dudas = $dudas;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }

    public function getAsesorId(): ?int
    {
        return $this->asesor_id;
    }

    public function setAsesorId(?int $asesor_id): self
    {
        $this->asesor_id = $asesor_id;

        return $this;
    }

    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    public function setClienteId(int $cliente_id): self
    {
        $this->cliente_id = $cliente_id;

        return $this;
    }

    public function getInmuebleId(): ?int
    {
        return $this->inmueble_id;
    }

    public function setInmuebleId(int $inmueble_id): self
    {
        $this->inmueble_id = $inmueble_id;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }
}
