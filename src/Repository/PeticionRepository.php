<?php

namespace App\Repository;

use App\Entity\Peticion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Peticion>
 *
 * @method Peticion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Peticion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Peticion[]    findAll()
 * @method Peticion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeticionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Peticion::class);
    }

    public function save(Peticion $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Peticion $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getPeticionCliente($idUser): ?array
    {
        //se hace un left join para obtener las peticiones aun cuando no tengan registrado un medico Id
        $strSql = "SELECT peticiones.id,
        peticiones.descripcion,
        peticiones.dudas,
        peticiones.estado,
        peticiones.tipo,
        inmuebles.descripcion inmueble,
        userAsesor.nombres asesor_nombres,
        userAsesor.apellidos asesor_apellidos,
        userCliente.nombres cliente_nombres,
        userCliente.apellidos cliente_apellidos
        FROM App\Entity\Peticion peticiones
        JOIN App\Entity\Usuario userCliente
        WITH peticiones.cliente_id = userCliente.id
        JOIN App\Entity\Inmueble inmuebles
        WITH peticiones.inmueble_id = inmuebles.id
        LEFT JOIN App\Entity\Usuario userAsesor
        WITH peticiones.asesor_id = userAsesor.id
        WHERE peticiones.cliente_id =:cliente AND peticiones.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('cliente',$idUser)
                    ->setParameter('estado',"A")
                    ->getResult();         
    }
    public function getPeticionAsesor(): ?array
    {
        //se hace un left join para obtener las peticiones aun cuando no tengan registrado un medico Id
        $strSql = "SELECT peticiones.id,
        peticiones.descripcion,
        peticiones.dudas,
        peticiones.estado,
        peticiones.tipo,
        inmuebles.descripcion inmueble,
        userAsesor.nombres asesor_nombres,
        userAsesor.apellidos asesor_apellidos,
        userCliente.nombres cliente_nombres,
        userCliente.apellidos cliente_apellidos
        FROM App\Entity\Peticion peticiones
        JOIN App\Entity\Usuario userCliente
        WITH peticiones.cliente_id = userCliente.id
        JOIN App\Entity\Inmueble inmuebles
        WITH peticiones.inmueble_id = inmuebles.id
        LEFT JOIN App\Entity\Usuario userAsesor
        WITH peticiones.asesor_id = userAsesor.id
        WHERE peticiones.estado =:estadob AND peticiones.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estadob','Solicitado')
                    ->setParameter('estado',"A")
                    ->getResult();         
    }

//    /**
//     * @return Peticion[] Returns an array of Peticion objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Peticion
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
