<?php

namespace App\Repository;

use App\Entity\Inmueble;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Inmueble>
 *
 * @method Inmueble|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inmueble|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inmueble[]    findAll()
 * @method Inmueble[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InmuebleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inmueble::class);
    }

    public function save(Inmueble $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Inmueble $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getInmuebleAsesor($idUser): ?array
    {
        //se hace un left join para obtener las inmuebles aun cuando no tengan registrado un medico Id
        $strSql = "SELECT inmuebles.id,
        inmuebles.descripcion,
        inmuebles.ubicacion,
        inmuebles.precio,
        inmuebles.estado,
        inmuebles.tipo,
        userAsesor.nombres asesor_nombres,
        userAsesor.apellidos asesor_apellidos,
        userCliente.nombres cliente_nombres,
        userCliente.apellidos cliente_apellidos
        FROM App\Entity\Inmueble inmuebles
        LEFT JOIN App\Entity\Usuario userCliente
        WITH inmuebles.cliente_id = userCliente.id
        LEFT JOIN App\Entity\Usuario userAsesor
        WITH inmuebles.asesor_id = userAsesor.id
        WHERE inmuebles.asesor_id =:asesor AND inmuebles.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('asesor',$idUser)
                    ->setParameter('estado',"A")
                    ->getResult();         
    }
    public function getInmuebles(): ?array
    {
        //se hace un left join para obtener las inmuebles aun cuando no tengan registrado un medico Id
        $strSql = "SELECT inmuebles.id,
                   inmuebles.descripcion,
                   inmuebles.ubicacion,
                   inmuebles.precio,
                   inmuebles.estado,
                   inmuebles.tipo,
                   userAsesor.nombres asesor_nombres,
                   userAsesor.apellidos asesor_apellidos,
                   userCliente.nombres cliente_nombres,
                   userCliente.apellidos cliente_apellidos
                   FROM App\Entity\Inmueble inmuebles
                   LEFT JOIN App\Entity\Usuario userCliente
                   WITH inmuebles.cliente_id = userCliente.id
                   JOIN App\Entity\Usuario userAsesor
                   WITH inmuebles.asesor_id = userAsesor.id
                   WHERE inmuebles.estado =:estado1 OR inmuebles.estado =:estado AND inmuebles.estado_base =:estadob";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado1',"En venta")
                    ->setParameter('estado',"En alquiler")
                    ->setParameter('estadob',"A")
                    ->getResult();         
    }         
    
    public function getInmuebleTipo($estado): ?array
    {
        //se hace un left join para obtener las inmuebles aun cuando no tengan registrado un medico Id
        $strSql = "SELECT inmuebles.id,
                   inmuebles.descripcion,
                   inmuebles.ubicacion,
                   inmuebles.precio,
                   inmuebles.estado,
                   inmuebles.tipo,
                   userAsesor.nombres asesor_nombres,
                   userAsesor.apellidos asesor_apellidos,
                   userCliente.nombres cliente_nombres,
                   userCliente.apellidos cliente_apellidos
                   FROM App\Entity\Inmueble inmuebles
                   LEFT JOIN App\Entity\Usuario userCliente
                   WITH inmuebles.cliente_id = userCliente.id
                   LEFT JOIN App\Entity\Usuario userAsesor
                   WITH inmuebles.asesor_id = userAsesor.id
                   WHERE inmuebles.tipo =:estado  AND inmuebles.estado_base =:estadob";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',$estado)
                    ->setParameter('estadob',"A")
                    ->getResult();         
    }
    public function getInmuebleAdmin(): ?array
    {
        //se hace un left join para obtener las inmuebles aun cuando no tengan registrado un medico Id
        $strSql = "SELECT inmuebles.id,
                   inmuebles.descripcion,
                   inmuebles.ubicacion,
                   inmuebles.precio,
                   inmuebles.estado,
                   inmuebles.tipo,
                   userAsesor.nombres asesor_nombres,
                   userAsesor.apellidos asesor_apellidos,
                   userCliente.nombres cliente_nombres,
                   userCliente.apellidos cliente_apellidos
                   FROM App\Entity\Inmueble inmuebles
                   LEFT JOIN App\Entity\Usuario userCliente
                   WITH inmuebles.cliente_id = userCliente.id
                   JOIN App\Entity\Usuario userAsesor
                   WITH inmuebles.asesor_id = userAsesor.id
                   WHERE  inmuebles.estado_base =:estado";
        return $this->_em->createQuery($strSql)                
                    ->setParameter('estado',"A")
                    ->getResult();         
    }

//    /**
//     * @return Inmueble[] Returns an array of Inmueble objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Inmueble
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
