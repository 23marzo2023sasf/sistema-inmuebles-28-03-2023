<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Entity\Inmueble;
use App\Form\RegisterInmuebleType;
use App\Repository\UsuarioRepository;
use App\Repository\InmuebleRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(Request $request, UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/index.html.twig', [
            'listUsuarios' => $usuarioRepository->findAll(),
        ]);
    }
    #[Route('/admin/dashboard/inmuebles', name: 'app_admin_dashboard_inmuebles')]
    public function inmuebles(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/inmuebles.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleAdmin(),
        ]);
    }
    
    #[Route('/admin/dashboard/edificios', name: 'app_admin_dashboard_edificios')]
    public function inmueblesEdificio(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/inmuebles.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleTipo("Edificio"),
        ]);
    }
    #[Route('/admin/dashboard/casas', name: 'app_admin_dashboard_casas')]
    public function inmueblesCasa(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/inmuebles.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleTipo("Casa"),
        ]);
    }
    #[Route('/admin/dashboard/departamento', name: 'app_admin_dashboard_departamento')]
    public function inmueblesDepartamento(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/inmuebles.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleTipo("Departamento"),
        ]);
    }
    #[Route('/admin/dashboard/register', name: 'app_admin_dashboard_register')]
    public function agregar(Request $request, InmuebleRepository $inmuebleRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        $inmueble=new Inmueble();
        $form =$this->createForm(RegisterInmuebleType::class, $inmueble);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $inmueble=$form->getData();
            $inmueble->setEstadoBase("A"); 
            $em=$doctrine->getManager();
            $em->persist($inmueble);
            $em->flush();
            $this->addFlash("success","Registro de Inmueble Exitoso");
            return $this->redirectToRoute('app_admin_dashboard');
        }
        return $this->render('admin_dashboard/registerInmueble.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/admin/dashboard/edit/{id}', name: 'app_admin_dashboard_edit')]
    public function edit(Request $request,Inmueble $inmueble, InmuebleRepository $inmuebleRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $UsuarioRepository->findOneBy(["email"=>$email]);
        
        $form = $this->createForm(RegisterPaqueteType::class, $inmueble);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if(($inmueble->getEstado() == "Adquirido")||($inmueble->getEstado() == "En alquiler")||($inmueble->getEstado() == "En venta"))    
        {
            $inmueble=$form->getData();
           
            $this->addFlash("success", "Exitos: El Inmueble fue editado con exito");
            $inmuebleRepository->save($inmueble,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar un inmueble que ya se ha vendido o alquilado"); 
        }
          return $this->redirectToRoute('app_admin_dashboard');  
        }
        return $this->render('admin_dashboard/registerInmueble.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }

    #[Route('/admin/dashboard/delete/{id}', name: 'app_admin_dashboard_delete')]
    public function delete(Inmueble $inmueble, InmuebleRepository $inmuebleRepository): Response
    {
       
            $inmueble-> setEstadoBase("I"); 
            $inmuebleRepository->save($inmueble,true);
        return $this->redirectToRoute('app_admin_dashboard');
    }
}
