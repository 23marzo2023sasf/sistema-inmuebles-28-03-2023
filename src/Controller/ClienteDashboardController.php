<?php

namespace App\Controller;

use App\Entity\Inmueble;
use App\Entity\Peticion;
use App\Form\RegisterPeticionType;
use App\Repository\InmuebleRepository;
use App\Repository\UsuarioRepository;
use App\Repository\PeticionRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClienteDashboardController extends AbstractController
{
    #[Route('/cliente/dashboard', name: 'app_cliente_dashboard')]
    public function index(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('cliente_dashboard/index.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebles(),
        ]);
    }
    #[Route('/cliente/dashboard/peticiones', name: 'app_cliente_dashboard_peticiones')]
    public function peticiones(Request $request, PeticionRepository $peticionRepository,UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('cliente_dashboard/peticiones.html.twig', [
            'listPeticiones' => $peticionRepository->getPeticionCliente($usuario->getId()),
        ]);
    }
    
    #[Route('/cliente/dashboard/register/{id}', name: 'app_cliente_dashboard_register')]
    public function agregar(Request $request, Inmueble $inmueble,PeticionRepository $peticionRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        $peticion=new Peticion();
        $form =$this->createForm(RegisterPeticionType::class, $peticion);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $peticion=$form->getData();
            $peticion->setEstadoBase("A"); 
            $peticion->setEstado("Solicitado");
            $peticion->setClienteId($user->getId());
            $peticion->setInmuebleId($inmueble->getId());
            $em=$doctrine->getManager();
            $em->persist($peticion);
            $em->flush();
            $this->addFlash("success","Registro de Peticion Exitoso");
            return $this->redirectToRoute('app_cliente_dashboard');
        }
        return $this->render('cliente_dashboard/registerPeticion.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/cliente/dashboard/edit/{id}', name: 'app_cliente_dashboard_edit')]
    public function edit(Request $request,Inmueble $peticion, PeticionRepository $peticionRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $UsuarioRepository->findOneBy(["email"=>$email]);
        
        $form = $this->createForm(RegisterPaqueteType::class, $peticion);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if(($peticion->getEstado() == "Solicitado"))    
        {
            $peticion=$form->getData();
            $this->addFlash("success", "Exitos: La Peticion fue editada con exito");
            $peticionRepository->save($peticion,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede editar una peticion que ya se ha sido aceptada"); 
        }
          return $this->redirectToRoute('app_cliente_dashboard');  
        }
        return $this->render('cliente_dashboard/registerPeticion.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }

    #[Route('/cliente/dashboard/delete/{id}', name: 'app_cliente_dashboard_delete')]
    public function delete(Peticion $peticion, PeticionRepository $peticionRepository): Response
    {
       
            $peticion-> setEstadoBase("I"); 
            $peticionRepository->save($peticion,true);
        return $this->redirectToRoute('app_cliente_dashboard');
    }
}
