<?php

namespace App\Controller;

use App\Entity\Inmueble;
use App\Entity\Peticion;
use App\Form\RegisterInmuebleType;
use App\Repository\UsuarioRepository;
use App\Repository\InmuebleRepository;
use App\Repository\PeticionRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AsesorDashboardController extends AbstractController
{
    #[Route('/asesor/dashboard', name: 'app_asesor_dashboard')]
    public function index(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('asesor_dashboard/index.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleAsesor($usuario->getId()),
        ]);
    }
    #[Route('/asesor/dashboard/edificios', name: 'app_asesor_dashboard_edificios')]
    public function inmueblesEdificio(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('asesor_dashboard/index.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleTipo("Edificio"),
        ]);
    }
    #[Route('/asesor/dashboard/casas', name: 'app_asesor_dashboard_casas')]
    public function inmueblesCasa(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('asesor_dashboard/index.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleTipo("Casa"),
        ]);
    }
    #[Route('/asesor/dashboard/departamento', name: 'app_asesor_dashboard_departamento')]
    public function inmueblesDepartamento(Request $request, InmuebleRepository $inmuebleRepository,UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('asesor_dashboard/index.html.twig', [
            'listInmuebles' => $inmuebleRepository->getInmuebleTipo("Departamento"),
        ]);
    }
    #[Route('/asesor/dashboard/edit/{id}', name: 'app_asesor_dashboard_edit')]
    public function edit(Request $request,Inmueble $inmueble, InmuebleRepository $inmuebleRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $UsuarioRepository->findOneBy(["email"=>$email]);
        
        $form = $this->createForm(RegisterInmuebleType::class, $inmueble,['accion'=>'asesor']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if(($inmueble->getEstado() == "Adquirido")||($inmueble->getEstado() == "En alquiler")||($inmueble->getEstado() == "En venta"))    
        {
            $inmueble=$form->getData();
            $inmueble->setasesorId($objUsuario->getId());
           
            $this->addFlash("success", "Exitos: El Inmueble fue editado con exito");
            $inmuebleRepository->save($inmueble,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar un inmueble que ya se ha vendido o alquilado"); 
        }
          return $this->redirectToRoute('app_asesor_dashboard');  
        }
        return $this->render('asesor_dashboard/registerInmueble.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
    #[Route('/asesor/dashboard/peticiones', name: 'app_asesor_dashboard_peticiones')]
    public function peticiones(Request $request, PeticionRepository $peticionRepository,UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('asesor_dashboard/peticionesSinAtender.html.twig', [
            'listPeticiones' => $peticionRepository->getPeticionAsesor(),
        ]);
    }
    #[Route('/asesor/dashboard/aceptar/{id}', name: 'app_asesor_dashboard_aceptar')]
    public function aceptar(Request $request,Peticion $peticion,UsuarioRepository $usuarioRepository, InmuebleRepository $inmuebleRepository,PeticionRepository $peticionRepository): Response
    {
       
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
            $peticion-> setEstado("Aceptada"); 
            $peticion->setAsesorId($usuario->getId());
            $inmueble=$inmuebleRepository->findOneBy(['id'=>$peticion->getInmuebleId()]);
            $inmueble->setClienteId($peticion->getClienteId());
            
            $peticionRepository->save($peticion,true);
            
            
        return $this->redirectToRoute('app_asesor_dashboard_peticiones');
    }
    #[Route('/asesor/dashboard/rechazar/{id}', name: 'app_asesor_dashboard_rechazar')]
    public function rechazar(Request $request,Peticion $peticion,UsuarioRepository $usuarioRepository, InmuebleRepository $inmuebleRepository,PeticionRepository $peticionRepository): Response
    {
       
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        $peticion->setAsesorId($usuario->getId());
             $peticion-> setEstado("Rechazada"); 
            $peticionRepository->save($peticion,true);
        return $this->redirectToRoute('app_asesor_dashboard_peticiones');
    }
}
